
# How to build & run locally
Screenshot: [IDE](https://gitlab.com/ligolang/ligo-web-ide/blob/master/src/pics/sshot.png)

Install beforehand: 
   - npm
   - yarn

Clone repo, then

```sh
$ yarn install
$ yarn workspace server run start
```

# How to add a new language

- Update custom monaco-languages repo at https://gitlab.com/ligolang/monaco-languages/
- Pull custom monaco-languages repo and manually build (with `npm install; npm run prepublishOnly`) to get release folder, push to git (create branch, push to branch, merge request)
- Manually build custom monaco-editor (with `npm install; npm run release`) to get release folder, push to git (create branch, push to branch, merge request)
- in root (here) run `rm -rf node_modules; npm install`
