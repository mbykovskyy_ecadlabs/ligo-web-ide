import {
  ActionType as ExamplesActionType,
  ChangeSelectedAction as ChangeSelectedExampleAction,
  DEFAULT_STATE as DEFAULT_EXAMPLES_STATE,
} from './examples';

export enum ActionType {
  ChangeEntrypoint = 'evaluate-value-change-entrypoint'
}

export interface EvaluateValueState {
  entrypoint: string;
}

export class ChangeEntrypointAction {
  public readonly type = ActionType.ChangeEntrypoint;
  constructor(public payload: EvaluateValueState['entrypoint']) {}
}

type Action = ChangeEntrypointAction | ChangeSelectedExampleAction;

const DEFAULT_STATE: EvaluateValueState = {
  ...DEFAULT_EXAMPLES_STATE.selected.evaluateValue
};

export default (state = DEFAULT_STATE, action: Action): EvaluateValueState => {
  switch (action.type) {
    case ExamplesActionType.ChangeSelected:
      return {
        ...state,
        ...action.payload.evaluateValue
      };
    case ActionType.ChangeEntrypoint:
      return {
        ...state,
        entrypoint: action.payload
      };
  }
  return state;
};
