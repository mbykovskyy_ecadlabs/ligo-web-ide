import {
  ActionType as ExamplesActionType,
  ChangeSelectedAction as ChangeSelectedExampleAction,
  DEFAULT_STATE as DEFAULT_EXAMPLES_STATE,
} from './examples';

export enum ActionType {
  ChangeEntrypoint = 'deploy-change-entrypoint',
  ChangeStorage = 'deploy-change-storage',
  UseTezBridge = 'deploy-use-tezbridge'
}

export interface DeployState {
  entrypoint: string;
  storage: string;
  useTezBridge: boolean;
}

export class ChangeEntrypointAction {
  public readonly type = ActionType.ChangeEntrypoint;
  constructor(public payload: DeployState['entrypoint']) {}
}

export class ChangeStorageAction {
  public readonly type = ActionType.ChangeStorage;
  constructor(public payload: DeployState['storage']) {}
}

export class UseTezBridgeAction {
  public readonly type = ActionType.UseTezBridge;
  constructor(public payload: DeployState['useTezBridge']) {}
}

type Action =
  | ChangeEntrypointAction
  | ChangeStorageAction
  | UseTezBridgeAction
  | ChangeSelectedExampleAction;

const DEFAULT_STATE: DeployState = {
  ...DEFAULT_EXAMPLES_STATE.selected.deploy
};

export default (state = DEFAULT_STATE, action: Action): DeployState => {
  switch (action.type) {
    case ExamplesActionType.ChangeSelected:
      return {
        ...state,
        ...action.payload.deploy
      };
    case ActionType.ChangeEntrypoint:
      return {
        ...state,
        entrypoint: action.payload
      };
    case ActionType.ChangeStorage:
      return {
        ...state,
        storage: action.payload
      };
    case ActionType.UseTezBridge:
      return {
        ...state,
        useTezBridge: action.payload
      };
  }
  return state;
};
