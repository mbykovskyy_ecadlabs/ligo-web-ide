import {
  ActionType as ExamplesActionType,
  ChangeSelectedAction as ChangeSelectedExampleAction,
  DEFAULT_STATE as DEFAULT_EXAMPLES_STATE,
} from './examples';
import { Language } from './types';

export enum ActionType {
  ChangeLanguage = 'editor-change-language',
  ChangeCode = 'editor-change-code'
}

export interface EditorState {
  language: Language;
  code: string;
}

export class ChangeLanguageAction {
  public readonly type = ActionType.ChangeLanguage;
  constructor(public payload: EditorState['language']) {}
}

export class ChangeCodeAction {
  public readonly type = ActionType.ChangeCode;
  constructor(public payload: EditorState['code']) {}
}

type Action =
  | ChangeCodeAction
  | ChangeLanguageAction
  | ChangeSelectedExampleAction;

const DEFAULT_STATE: EditorState = {
  ...DEFAULT_EXAMPLES_STATE.selected.editor
};

export default (state = DEFAULT_STATE, action: Action): EditorState => {
  switch (action.type) {
    case ExamplesActionType.ChangeSelected:
      return {
        ...state,
        ...action.payload.editor
      };
    case ActionType.ChangeLanguage:
      return {
        ...state,
        language: action.payload
      };
    case ActionType.ChangeCode:
      return {
        ...state,
        code: action.payload
      };
  }
  return state;
};
