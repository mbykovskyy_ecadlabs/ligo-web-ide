import { CompileState } from './compile';
import { DeployState } from './deploy';
import { DryRunState } from './dry-run';
import { EditorState } from './editor';
import { EvaluateFunctionState } from './evaluate-function';
import { EvaluateValueState } from './evaluate-value';
import { Language } from './types';

export interface ExampleState {
  id: number;
  name: string;
  editor: EditorState;
  compile: CompileState;
  dryRun: DryRunState;
  deploy: DeployState;
  evaluateFunction: EvaluateFunctionState;
  evaluateValue: EvaluateValueState;
}

export const PASCALIGO_EXAMPLE = {
  id: 0,
  name: 'PascaLIGO Contract',
  editor: {
    code: `// variant defining pseudo multi-entrypoint actions
type action is
| Increment of int
| Decrement of int

function add (const a : int ; const b : int) : int is
  block { skip } with a + b

function subtract (const a : int ; const b : int) : int is
  block { skip } with a - b

// real entrypoint that re-routes the flow based
// on the action provided
function main (const p : action ; const s : int) :
  (list(operation) * int) is
  block { skip } with ((nil : list(operation)),
  case p of
  | Increment(n) -> add(s, n)
  | Decrement(n) -> subtract(s, n)
  end)`,
    language: Language.PascaLigo
  },
  compile: {
    entrypoint: 'main'
  },
  dryRun: {
    entrypoint: 'main',
    parameters: 'Increment (5)',
    storage: '1'
  },
  deploy: {
    entrypoint: 'main',
    storage: '1',
    useTezBridge: false
  },
  evaluateFunction: {
    entrypoint: 'add',
    parameters: '(5, 6)'
  },
  evaluateValue: {
    entrypoint: ''
  }
};

export const CAMELIGO_EXAMPLE = {
  id: 1,
  name: 'CameLIGO Contract',
  editor: {
    code: `type storage = int

(* variant defining pseudo multi-entrypoint actions *)
type action =
  | Increment of int
  | Decrement of int

let add (a: int) (b: int): int = a + b

let subtract (a: int) (b: int): int = a - b

(* real entrypoint that re-routes the flow based on 
    the action provided *)
let%entry main(p : action) storage =
  let storage =
    match p with
    | Increment n -> add storage n
    | Decrement n -> subtract storage n
  in (([] : operation list), storage)`,
    language: Language.CameLigo
  },
  compile: {
    entrypoint: 'main'
  },
  dryRun: {
    entrypoint: 'main',
    parameters: 'Increment 5',
    storage: '1'
  },
  deploy: {
    entrypoint: 'main',
    storage: '1',
    useTezBridge: false
  },
  evaluateFunction: {
    entrypoint: 'add',
    parameters: '5, 6'
  },
  evaluateValue: {
    entrypoint: ''
  }
};
