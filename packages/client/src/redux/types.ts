export enum Language {
  PascaLigo = 'pascaligo',
  CameLigo = 'cameligo'
}

export enum Command {
  Compile = 'compile',
  DryRun = 'dry-run',
  EvaluateValue = 'evaluate-value',
  EvaluateFunction = 'evaluate-function',
  Deploy = 'deploy'
}
