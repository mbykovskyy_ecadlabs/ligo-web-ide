import { CAMELIGO_EXAMPLE, ExampleState, PASCALIGO_EXAMPLE } from './example';

export enum ActionType {
  ChangeSelected = 'examples-change-selected'
}

export interface ExamplesState {
  selected: ExampleState;
  list: ExampleState[];
}

export class ChangeSelectedAction {
  public readonly type = ActionType.ChangeSelected;
  constructor(public payload: ExamplesState['selected']) {}
}

type Action = ChangeSelectedAction;

export const DEFAULT_STATE: ExamplesState = {
  selected: { ...PASCALIGO_EXAMPLE },
  list: [{ ...PASCALIGO_EXAMPLE }, { ...CAMELIGO_EXAMPLE }]
};

export default (state = DEFAULT_STATE, action: Action): ExamplesState => {
  switch (action.type) {
    case ActionType.ChangeSelected:
      return {
        ...state,
        selected: action.payload
      };
  }
  return state;
};
