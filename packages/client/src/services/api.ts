import axios from 'axios';

import { AppState } from '../redux/app';
import { Language } from '../redux/types';

export async function compileContract(
  syntax: Language,
  code: string,
  entrypoint: string,
  format?: string
) {
  const response = await axios.post('/api/compile-contract', {
    syntax,
    code,
    entrypoint,
    format
  });
  return response.data;
}

export async function compileExpression(
  syntax: Language,
  expression: string,
  format?: string
) {
  const response = await axios.post('/api/compile-expression', {
    syntax,
    expression,
    format
  });
  return response.data;
}

export async function dryRun(
  syntax: Language,
  code: string,
  entrypoint: string,
  parameter: string,
  storage: string
) {
  const response = await axios.post('/api/dry-run', {
    syntax,
    code,
    entrypoint,
    parameter,
    storage
  });
  return response.data;
}

export async function share({
  editor,
  compile,
  dryRun,
  deploy,
  evaluateValue,
  evaluateFunction
}: Partial<AppState>) {
  const response = await axios.post('/api/share', {
    editor,
    compile,
    dryRun,
    deploy,
    evaluateValue,
    evaluateFunction
  });
  return response.data;
}

export async function deploy(
  syntax: Language,
  code: string,
  entrypoint: string,
  storage: string
) {
  const response = await axios.post('/api/deploy', {
    syntax,
    code,
    entrypoint,
    storage
  });
  return response.data;
}

export async function evaluateValue(
  syntax: Language,
  code: string,
  entrypoint: string
) {
  const response = await axios.post('/api/evaluate-value', {
    syntax,
    code,
    entrypoint
  });
  return response.data;
}

export async function runFunction(
  syntax: Language,
  code: string,
  entrypoint: string,
  parameter: string
) {
  const response = await axios.post('/api/run-function', {
    syntax,
    code,
    entrypoint,
    parameter
  });
  return response.data;
}

export function getErrorMessage(ex: any): string {
  if (ex.response && ex.response.data) {
    return ex.response.data.error;
  } else if (ex instanceof Error) {
    return ex.message;
  }

  return JSON.stringify(ex);
}
