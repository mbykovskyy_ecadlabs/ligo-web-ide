const commonUtils = require('./common-utils');

const API_HOST = commonUtils.API_HOST;
const API_ROOT = commonUtils.API_ROOT;
const CAMELIGO_CODE = commonUtils.CAMELIGO_CODE;

const clearText = commonUtils.clearText;
const getInnerText = commonUtils.getInnerText;
const waitForResponse = commonUtils.waitForResponse;

describe('Compile contract', () => {
  beforeAll(() => jest.setTimeout(60000));

  beforeEach(async () => {
    await page.goto(API_HOST);
  });

  test('should compile PascaLIGO', async done => {
    await page.click('#run');
    await waitForResponse(page, `${API_ROOT}/compile-contract`);
    const code = await page.evaluate(getInnerText, 'output');

    const expectedCode =
      '{ parameter (or (int %decrement) (int %increment)) ; storage int ; code { LAMBDA (pair int int) int { DUP ; CAR ; DIP { DUP } ; SWAP ; CDR ; DIP { DUP } ; SWAP ; DIP { DUP } ; ADD ; DIP { DROP 3 } } ; LAMBDA (pair int int) int { DUP ; CAR ; DIP { DUP } ; SWAP ; CDR ; DIP { DUP } ; SWAP ; DIP { DUP } ; SUB ; DIP { DROP 3 } } ; DIP 2 { DUP } ; DIG 2 ; CAR ; DIP 3 { DUP } ; DIG 3 ; CDR ; DIP { DUP } ; SWAP ; IF_LEFT { DUP ; DIP 2 { DUP } ; DIG 2 ; DIP { DUP } ; PAIR ; DIP { DIP 4 { DUP } ; DIG 4 } ; EXEC ; DIP { DROP 2 } } { DUP ; DIP 2 { DUP } ; DIG 2 ; DIP { DUP } ; PAIR ; DIP { DIP 5 { DUP } ; DIG 5 } ; EXEC ; DIP { DROP 2 } } ; NIL operation ; PAIR ; DIP { DROP 5 } } } ';

    expect(code.replace(/\s|\n|\t/g, '')).toEqual(
      expectedCode.replace(/\s|\n|\t/g, '')
    );
    done();
  });

  it('should compile CameLIGO', async done => {
    await page.click('#editor');
    await clearText(page.keyboard);
    await page.keyboard.type(CAMELIGO_CODE);

    await page.click('#syntax-select');
    await page.click('#cameligo');
    await page.click('#run');

    await waitForResponse(page, `${API_ROOT}/compile-contract`);
    const code = await page.evaluate(getInnerText, 'output');

    const expectedCode =
      '{ parameter (or (int %decrement) (int %increment)) ; storage int ; code { LAMBDA (pair int int) int { DUP ; CAR ; DIP { DUP } ; SWAP ; CDR ; DIP { DUP } ; SWAP ; DIP { DUP } ; ADD ; DIP { DROP 3 } } ; LAMBDA (pair int int) int { DUP ; CAR ; DIP { DUP } ; SWAP ; CDR ; DIP { DUP } ; SWAP ; DIP { DUP } ; SUB ; DIP { DROP 3 } } ; DIP 2 { DUP } ; DIG 2 ; CAR ; DIP 3 { DUP } ; DIG 3 ; CDR ; DIP { DUP } ; SWAP ; IF_LEFT { DUP ; DIP 2 { DUP } ; DIG 2 ; DIP { DUP } ; PAIR ; DIP { DIP 4 { DUP } ; DIG 4 } ; EXEC ; DIP { DROP 2 } } { DUP ; DIP 2 { DUP } ; DIG 2 ; DIP { DUP } ; PAIR ; DIP { DIP 5 { DUP } ; DIG 5 } ; EXEC ; DIP { DROP 2 } } ; DUP ; NIL operation ; PAIR ; DIP { DROP 6 } } }';

    expect(code.replace(/\s|\n|\t/g, '')).toEqual(
      expectedCode.replace(/\s|\n|\t/g, '')
    );
    done();
  });

  it('should return an error when entrypoint is blank', async done => {
    await page.click('#command-select');
    await page.click('#compile');

    await page.click('#entrypoint');
    await clearText(page.keyboard);

    await page.click('#run');
    await waitForResponse(page, `${API_ROOT}/compile-contract`);

    const actualOutput = await page.evaluate(getInnerText, 'output');

    const expectedOutput = 'Error: "entrypoint" is not allowed to be empty';

    expect(actualOutput).toEqual(expectedOutput);
    done();
  });

  it('should return an error when code has compilation error', async done => {
    await page.click('#editor');
    await page.keyboard.type('asdf');

    await page.click('#run');
    await waitForResponse(page, `${API_ROOT}/compile-contract`);

    const actualOutput = await page.evaluate(getInnerText, 'output');

    const expectedOutput = 'Error: ';

    expect(actualOutput).toEqual(expectedOutput);
    done();
  });
});
