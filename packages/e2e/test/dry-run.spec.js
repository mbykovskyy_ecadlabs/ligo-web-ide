const commonUtils = require('./common-utils');

const API_HOST = commonUtils.API_HOST;
const API_ROOT = commonUtils.API_ROOT;
const CAMELIGO_CODE = commonUtils.CAMELIGO_CODE;

const clearText = commonUtils.clearText;
const getInnerText = commonUtils.getInnerText;
const waitForResponse = commonUtils.waitForResponse;

describe('Dry run contract', () => {
  beforeAll(() => jest.setTimeout(60000));

  beforeEach(async () => {
    await page.goto(API_HOST);
  });

  it('should run PascaLIGO and return correct value', async done => {
    await page.click('#command-select');
    await page.click('#dry-run');

    await page.click('#run');
    await waitForResponse(page, `${API_ROOT}/dry-run`);

    const actualOutput = await page.evaluate(getInnerText, 'output');

    const expectedOutput = 'tuple[list[]6]';

    expect(actualOutput.replace(/\s|\n|\t/g, '')).toEqual(
      expectedOutput.replace(/\s|\n|\t/g, '')
    );
    done();
  });

  it('should run CameLIGO and return correct value', async done => {
    await page.click('#editor');
    await clearText(page.keyboard);
    await page.keyboard.type(CAMELIGO_CODE);

    await page.click('#syntax-select');
    await page.click('#cameligo');

    await page.click('#command-select');
    await page.click('#dry-run');

    await page.click('#run');
    await waitForResponse(page, `${API_ROOT}/dry-run`);

    const actualOutput = await page.evaluate(getInnerText, 'output');

    const expectedOutput = 'tuple[list[]6]';

    expect(actualOutput.replace(/\s|\n|\t/g, '')).toEqual(
      expectedOutput.replace(/\s|\n|\t/g, '')
    );
    done();
  });

  it('should return an error when entrypoint is blank', async done => {
    await page.click('#command-select');
    await page.click('#dry-run');

    await page.click('#entrypoint');
    await clearText(page.keyboard);

    await page.click('#run');
    await waitForResponse(page, `${API_ROOT}/dry-run`);

    const actualOutput = await page.evaluate(getInnerText, 'output');

    const expectedOutput = 'Error: "entrypoint" is not allowed to be empty';

    expect(actualOutput).toEqual(expectedOutput);
    done();
  });

  it('should return an error when parameter is blank', async done => {
    await page.click('#command-select');
    await page.click('#dry-run');

    await page.click('#parameters');
    await clearText(page.keyboard);

    await page.click('#run');
    await waitForResponse(page, `${API_ROOT}/dry-run`);

    const actualOutput = await page.evaluate(getInnerText, 'output');

    const expectedOutput = 'Error: "parameter" is not allowed to be empty';

    expect(actualOutput).toEqual(expectedOutput);
    done();
  });

  it('should return an error when storage is blank', async done => {
    await page.click('#command-select');
    await page.click('#dry-run');

    await page.click('#storage');
    await clearText(page.keyboard);

    await page.click('#run');
    await waitForResponse(page, `${API_ROOT}/dry-run`);

    const actualOutput = await page.evaluate(getInnerText, 'output');

    const expectedOutput = 'Error: "storage" is not allowed to be empty';

    expect(actualOutput).toEqual(expectedOutput);
    done();
  });

  it('should return an error when code has compilation error', async done => {
    await page.click('#editor');
    await page.keyboard.type('asdf');

    await page.click('#command-select');
    await page.click('#dry-run');

    await page.click('#run');
    await waitForResponse(page, `${API_ROOT}/dry-run`);

    const actualOutput = await page.evaluate(getInnerText, 'output');

    const expectedOutput = 'Error: ';

    expect(actualOutput).toEqual(expectedOutput);
    done();
  });
});
