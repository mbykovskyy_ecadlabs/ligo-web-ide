exports.API_HOST = 'http://127.0.0.1:8080';
exports.API_ROOT = `${exports.API_HOST}/api`;

exports.CAMELIGO_CODE = `type storage = int
type action =
  | Increment of int
  | Decrement of int

let add (a: int) (b: int): int = a + b

let subtract (a: int) (b: int): int = a - b

let%entry main(p : action) storage =
  let storage =
    match p with
    | Increment n -> add storage n
    | Decrement n -> subtract storage n
  in (([] : operation list), storage)`;

exports.clearText = async keyboard => {
  await keyboard.down('Shift');
  for (let i = 0; i < 20; i++) {
    await keyboard.press('ArrowUp');
  }
  await keyboard.up('Shift');
  await keyboard.press('Backspace');
  await keyboard.down('Shift');
  for (let i = 0; i < 20; i++) {
    await keyboard.press('ArrowDown');
  }
  await keyboard.up('Shift');
  await keyboard.press('Backspace');
};

exports.waitForResponse = (page, url) => {
  return new Promise(resolve => {
    page.on('response', function callback(response) {
      if (response.url() === url) {
        resolve(response);
        page.removeListener('response', callback);
      }
    });
  });
};

exports.getInnerText = id => {
  let element = document.getElementById(id);
  return element && element.textContent;
};

exports.getInputValue = id => {
  let element = document.getElementById(id);
  return element && element.value;
};
