import { Request, Response } from 'express';

import latestSchema from '../schemas/share-latest';
import { storage } from '../storage';
import { FileNotFoundError } from '../storage/interface';

export function createSharedLinkHandler(template: (state: string) => string) {
  return async (req: Request, res: Response) => {
    try {
      const content = await storage.read(`${req.params['hash']}.txt`);
      const storedState = JSON.parse(content);
      const migratedState = latestSchema.forward(storedState);

      const state = {
        ...migratedState.state,
        share: { link: req.params['hash'] }
      };

      res.send(template(JSON.stringify(state)));
    } catch (ex) {
      if (ex instanceof FileNotFoundError) {
        res.sendStatus(404);
      } else {
        res.sendStatus(500);
      }
    }
  };
}
