import joi from '@hapi/joi';
import { Tezos } from '@taquito/taquito';
import { Request, Response } from 'express';
import fs from 'fs';

import { CompilerError, LigoCompiler } from '../ligo-compiler';
import { logger } from '../logger';

interface DeployBody {
  syntax: string;
  code: string;
  entrypoint: string;
  storage: string;
}

try {
  const FAUCET_KEY_FILE_ENV_VAR = 'FAUCET_KEY_FILE';
  const faucetKeyFile = process.env[FAUCET_KEY_FILE_ENV_VAR];

  if (!faucetKeyFile) {
    throw new Error(
      `${FAUCET_KEY_FILE_ENV_VAR} environmental variable is undefined`
    );
  }

  const faucetKey = fs.readFileSync(faucetKeyFile);

  const faucet = JSON.parse(faucetKey.toString());

  Tezos.setProvider({ rpc: 'https://api.tez.ie/rpc/babylonnet' });
  Tezos.importKey(
    faucet.email,
    faucet.password,
    faucet.mnemonic.join(' '),
    faucet.secret
  );
} catch (err) {
  logger.error(err);
  throw err;
}

const validateRequest = (body: any): { value: DeployBody; error: any } => {
  return joi
    .object({
      syntax: joi.string().required(),
      code: joi.string().required(),
      entrypoint: joi.string().required(),
      storage: joi.string().required()
    })
    .validate(body);
};

export async function deployHandler(req: Request, res: Response) {
  const { error, value: body } = validateRequest(req.body);

  if (error) {
    res.status(400).json({ error: error.message });
  } else {
    try {
      const michelsonCode = await new LigoCompiler().compileContract(
        body.syntax,
        body.code,
        body.entrypoint,
        'json'
      );

      const michelsonStorage = await new LigoCompiler().compileExpression(
        body.syntax,
        body.storage,
        'json'
      );

      const op = await Tezos.contract.originate({
        code: JSON.parse(michelsonCode),
        init: JSON.parse(michelsonStorage)
      });

      const contract = await op.contract();

      res.send({ ...contract });
    } catch (ex) {
      if (ex instanceof CompilerError) {
        res.status(400).json({ error: ex.message });
      } else {
        logger.error(ex);
        res.sendStatus(500);
      }
    }
  }
}
