FROM node:12-alpine as builder

WORKDIR /app

COPY package.json package.json
COPY yarn.lock yarn.lock
COPY packages/client packages/client
COPY packages/server packages/server

RUN yarn install

COPY tsconfig.json tsconfig.json

RUN yarn workspaces run build

FROM node:12-buster

WORKDIR /app

RUN apt-get update
RUN apt-get -y install libev-dev perl pkg-config libgmp-dev libhidapi-dev m4 libcap-dev bubblewrap rsync
RUN curl -o /tmp/ligo.deb https://gitlab.com/ligolang/ligo/-/jobs/341183878/artifacts/raw/dist/package/debian-10/ligo_341183878-5e44be6b_all.deb
RUN dpkg -i /tmp/ligo.deb
RUN rm /tmp/ligo.deb

COPY --from=builder /app/packages/client/build /app/client/build
COPY --from=builder /app/node_modules /app/node_modules
COPY --from=builder /app/packages/server/dist/src /app/server/dist

ENV STATIC_ASSETS /app/client
ENV LIGO_CMD /bin/ligo

ENTRYPOINT [ "node", "server/dist/index.js" ]
